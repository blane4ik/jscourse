function getDayDeclension(numberOfDays) {  
	if (numberOfDays % 10 === 1 && 
			numberOfDays !== 11) {
		console.log('День');
	} else if ((numberOfDays % 10) > 1 && 
						(numberOfDays % 10) < 5) {
		console.log('Дня');
	} else{
		console.log('Дней');
	}
};

getDayDeclension(2);
getDayDeclension(1);
getDayDeclension(7);