function addMinutes(hours, minutes, addingMinutes) {
	var newMinutes = (minutes + addingMinutes) % 60;
	var newHours = (hours + Math.floor((minutes + addingMinutes) / 60)) % 24;

	if (newHours < 10) {
		newHours = '0' + newHours;
	};

	if (newMinutes < 10) {
		newMinutes = '0' + newMinutes;
	}
	console.log(newHours + ':' + newMinutes);
};

addMinutes(23, 35, 33);
addMinutes(12, 55, 49);
addMinutes(9, 12, 117);