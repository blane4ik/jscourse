console.log('тип для числа: ' + typeof 1);
console.log('тип для строки: ' + typeof '2');
console.log('тип для булевого: ' + typeof true);
console.log('тип для undefined: ' + typeof undefined);
console.log('тип для null: ' + typeof null);
console.log('тип для object: ' + typeof {});
console.log('тип для function: ' + typeof function(){});
console.log('Максимальное число для переменной: ' + Number.MAX_SAFE_INTEGER);
console.log('Максимальное число для переменной: ' + Number.MAX_VALUE);

var a = 1 / 'foo';
console.log('1 / "foo" используя isNaN: ' + isNaN(a));

var b = 0 / 1;
console.log('0 / 1 используя isFinite: ' + isFinite(b));