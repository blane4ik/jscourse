function isPointInCircle(x, y) {
	var XC = 3,
			YC = 5,
			R = 4;

	if ( (x - XC)*(x - XC) +
		 (y - YC)*(y - YC) <= R*R){
		console.log('true');
	} else {
		console.log('false');
	}
};

console.log('Результат работы функции круга:');
isPointInCircle(3, 5);
isPointInCircle(0, 0);
isPointInCircle(3, 1);

function isPointInSquare(x, y) {
	var LINE1 = -0.6 * x + 3,
			LINE2 = 4 / 7 * x + 4,
			LINE3 = 0.4 * x - 2,
			LINE4 = -1.5 * x - 12;

			if (y <= LINE1 && y <= LINE2 &&
					y >= LINE3 && y >= LINE4) {
				console.log('true');
			} else {
				console.log('false');
			}
};
console.log('\nРезультат работы функции четырехугольника:');
isPointInSquare(1, 1);
isPointInSquare(-6, -1);
isPointInSquare(0, -5);